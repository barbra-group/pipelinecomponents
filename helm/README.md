# HELM

A alpine based docker image with:
 - kubectl (v1.14.1)
 - helm (v2.13.0)
 - envsubst
